#9. На вход функции my_func() подается список целых чисел. 
#На выходе выполнения функци получить кортеж уникальных элементов списка в обратном порядке.

from typing import List, Tuple

def my_func(nums: List[int]) -> Tuple[int]:
    uniqueElements = []
    for item in nums:
        if nums.count(item) == 1:
            uniqueElements.insert(0, item)
    return tuple(uniqueElements)

print(my_func([1,2,3,4,5,5,6,7,2]))
