#16. Напишите функцию sum_range(a, z), 
# которая перемножает все целые числа от значения a до величины z включительно

from Taska15 import sum_range


def mult_range(a: int, z: int) -> int:
    m = 1
    for i in range(z+1):
        m *=i 
    return m
