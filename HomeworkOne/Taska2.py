# 2. Декоратор считающий время выполнения функции.
from collections.abc import Callable
from typing import Tuple, TypeVar
from time import perf_counter_ns

def GetExecuteTime(func: Callable) -> Callable:
    #вернет резульат функции и время выполнения в наносекундах
    def wrapper(*args, **kwargs) -> Tuple[TypeVar, int]:
        startTime = perf_counter_ns()
        funcResult = func(*args, **kwargs)
        return  funcResult, (perf_counter_ns()-startTime)

    return wrapper

#пример использования
#нумерция чисел фибоначи начинается с нуля
# 0 1 2 3 4 5 6 7...
# 0 1 1 2 3 5 8 13...

@GetExecuteTime
def fib(n: int) -> int:
    fibs = [0,1]

    if n < 1 :
        return fibs[n]

    for _ in range(n - 1):
        fibs.append(fibs.pop(0)+fibs[-1])
   
    return fibs[-1]

if __name__ == '__main__':
    resultNum, executeTime = fib(1000)
    print("Время вычисления: ", executeTime, "нс.", "\nполученное число: ", resultNum)