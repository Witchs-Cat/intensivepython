#11. Написать функцию date, принимающую 3 аргумента — день, месяц и год. 
#Вернуть True, если такая дата есть в нашем календаре, и False иначе

#подразумевается что отсчет месяцев и дней идет с 1

def getDaysInMonth(month, year):
    def checkLeapYear(year):
        if year % 4 != 0:
            return False
        if year % 100 == 0 and year % 400 != 0:
            return False
        return True

    if checkLeapYear(year):
        return [-1,31,29,31,30,31,30,31,31,30,31,30,31][month]
    else:
        return [-1,31,28,31,30,31,30,31,31,30,31,30,31][month]

def date(day: int, month: int, year: int) -> bool:
    if month > 12 or month < 1:
        return False
    
    if day < 1 or day > getDaysInMonth(month, year):
        return False
    
    if year < 0:
        return False
    
    return True


    
    


    