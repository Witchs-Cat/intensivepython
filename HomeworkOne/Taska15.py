#Напишите функцию sum_range(a, z), 
# которая суммирует все целые числа от значения a до величины z включительно

def sum_range(a: int, z: int) -> int:
    return sum(range(a,z+1))
