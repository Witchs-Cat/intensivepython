#10. Напишите функцию tuple_sort(), которая сортирует кортеж,
# состоящий из целых чисел по возрастанию и возвращает его.

from typing import Tuple

def tuple_sort(nums: Tuple[int]) -> Tuple[int]:
    sortableList = list(nums)
    for i in range(len(sortableList)-1):
        for j in range(len(sortableList)-i-1):
            if sortableList[j] > sortableList[j+1]:
                sortableList[j], sortableList[j+1] = sortableList[j+1], sortableList[j]
    return tuple(sortableList)
