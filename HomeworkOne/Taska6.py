#6. Написать функцию, которая сливает два словаря в один.
from typing import Dict
from Taska5 import genDict

def merge(dictionry1: Dict, dictionary2: Dict) -> Dict:
    result = {}
    for dict in [dictionry1, dictionary2]:
        for key in dict:
            result[key] = dict[key]
    return result

if __name__ == '__main__':
    d1 = genDict(5)
    d2 = genDict(3)
    print("Первый: ", d1)
    print("Второй: ", d2)
    print("Результирующий: ", merge(d1,d2))