#1. Декоратор считающий количество вызовов функции.

from collections.abc import Callable
from typing import Tuple, TypeVar, Dict

 
#Честно, не знал как лучше реализовать. Не хотел чтоб поле __callsCount
#висело в глобальной области видимости
#поэтому решали сделать так

class FibCallsCounter:
    def __init__(self) -> None:
        self.__callsCount = 0

    def GetCallsCount(self) -> int:
        return self.__callsCount

    @staticmethod
    def __addCallsCount(func: Callable) -> Callable:
        def wrapper(self, *args: Tuple, **kwargs: Dict) -> Tuple[TypeVar, int]:
            self.__callsCount += 1
            return  func(self, *args, **kwargs)
        return wrapper

    #нумерция чисел фибоначи начинается с нуля
    # 0 1 2 3 4 5 6 7...
    # 0 1 1 2 3 5 8 13...
    @__addCallsCount
    def fib(self, n: int) -> int:
        if n < 1 :
            return 0
        if n < 2:
            return 1
        return self.fib(n-1) + self.fib(n-2)

if __name__ == '__main__':
    counter = FibCallsCounter()
    resultNum = counter.fib(3)
    callsCount = counter.GetCallsCount()
    print("Количество вызовов: ", callsCount, "\nПолученное число: ", resultNum)