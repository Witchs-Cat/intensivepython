#8. Напишите функцию reverse_my_list(lst), которая принимает список и меняет местами его первый и последний элемент.

from typing import List

def reverse_my_list(lst: List) -> List:
    lst[0], lst[-1] = lst[-1], lst[0]
    return lst
