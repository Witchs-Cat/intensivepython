# На входе функция list_to_set() получает список. Преобразуйте их в множество.
from typing import List, Set

def list_to_set(list: List) -> Set:
    return set(list)