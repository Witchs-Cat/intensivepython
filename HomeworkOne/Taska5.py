#5. Найдите три ключа с самыми высокими значениями в словаре my_dict
from typing import Dict, Optional
from random import randint

def genDict(length: Optional[int] = 10) -> Dict:
    def genKey() -> str:
        key = ""
        for i in range(5):
            key += chr(randint(97, 122))
        return key

    dictionary = {}
    for i in range(length):
        dictionary[genKey()] = randint(0,1000)
    return dictionary

if __name__ == '__main__':
    my_dict = genDict()
    print("изначальный словарь:\n", my_dict)
    
    sortedKeys = sorted(my_dict, key = lambda x: my_dict[x], reverse= True)
    maxsDict = {}
    for i in range(3):
        key = sortedKeys[i]
        maxsDict[key] = my_dict[key]
    print("Максимальные значения:\n",maxsDict)


