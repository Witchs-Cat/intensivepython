#17-18. Повторить уроки и выполнить с наложением своей (любой) 
#предметной области вместо заданной в уроках  
#https://highload.today/python-obektno-orientirovannoe-programmirovanie-oop-1/
#https://highload.today/python-obektno-orientirovannoe-programmirovanie-oop-praktika-2/

from inspect import istraceback


class Monkey:

    def __init__(self, name: str, age: int) -> None:
        self.name = name
        self.age = age

    def climb_a_tree(self):
        print("ya-a-a")

class Human(Monkey):

    def __init__(self, name: str, age: int) -> None:
        super().__init__(name, age)


    def say(self, msg: str) -> None:
        print(msg)
    
    def climb_a_tree(self):
        print("Мне лень")

h = Human("Кирил", 20)
m = Monkey("Джигурда", 10)
h.climb_a_tree()
m.climb_a_tree()

if (isinstance(h, Monkey)):
    print("Человек это примат")

if (not isinstance(m, Human)):
    print("Не каждый примат - человек")