#Напишите функцию, которая принимает два списка и выводит все элементы первого, которых нет во втором.
from typing import List

def printFLIO(list1: List, list2: List) -> None:
    for item in list1:
        if item not in list2:
            print(item)

#пример использования
list1 = [1,2,3,4,5]
list2 = [4,5,6,7,8]
printFLIO(list1, list2)