#13. На входе функция string_to_set() получает строку. Преобразуйте их в множество.
from typing import Set

def string_to_set(string: str) -> Set[str]:
    return set(string)
